<?php
\Route::group(['prefix' => 'sitemap', 'middleware' => 'secure'], function() {

    Route::any('/', array('as' => 'app.sitemap', 'uses' => 'SitemapController@make'));
});