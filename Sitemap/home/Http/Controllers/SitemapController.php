<?php
namespace APPLICATION_HOME\Http\Controllers;

class SitemapController extends ModuleController {

    public function __construct() {

        $this->middleware('auth');
    }

    public function make() {

        $sitemap = new STALKER_CMS\Packages\Sitemap\Sitemap(url('/'));
        $sitemap->setPath(public_path('/'));
        $sitemap->setFilename('sitemap');
        if(\PermissionsController::isPackageEnabled('core_content')):
            foreach(\PublicPage::getPagesList() as $page):
                $sitemap->addItem(substr($page->PageRelativeUrl, 1), '1.0', 'daily', 'Today');
            endforeach;
        endif;
        if(\PermissionsController::isPackageEnabled('solutions_news')):
            $news_segment = settings(['solutions_news', 'news', 'first_segment']);
            foreach(\PublicNews::getNews() as $news):
                $sitemap->addItem('/'.$news_segment.'/'.$news->PageUrl, '0.8', 'daily', 'Today');
            endforeach;
        endif;
        if(\PermissionsController::isPackageEnabled('solutions_events')):
            $events_segment = settings(['solutions_events', 'events', 'first_segment']);
            foreach(\PublicEvents::getEvents() as $event):
                $sitemap->addItem('/'.$events_segment.'/'.$event->PageUrl, '0.8', 'daily', 'Today');
            endforeach;
        endif;
        $sitemap->createSitemapIndex(url('/'), 'Today');
        return \Response::download(public_path('sitemap.xml'), 'sitemap.xml', ['Content-type' => 'text/xml']);
    }
}
